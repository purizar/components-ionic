import { Component } from '@angular/core';
import { Componente } from './interfaces/interfaces';
import { Observable } from 'rxjs';
import { MenuController, Platform } from '@ionic/angular';
import { DataService } from './services/data.service';
import { SplashScreenPluginWeb, StatusBarStyleOptions } from '@capacitor/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  componentes: Observable<Componente[]>;
  
  constructor(/*private platform: Platform,
              private splashScreem: SplashScreenPluginWeb,
              private statusBar: StatusBarStyleOptions,*/
              private dataService: DataService
  ) {
      this.initializeApp();
  }

  initializeApp() {
    /*this.platform.ready().then(()=>{
          //this.statusBar.style();
          this.splashScreem.hide();
    })*/
    this.componentes= this.dataService.getMenuOpts();
  }
}
