import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalInfoPage } from '../modal-info/modal-info.page';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  constructor( private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  async mostrarModal(){
    const modal = await this.modalCtrl.create({
      component: ModalInfoPage,
      componentProps:{
        nombre:"Paola",
        pais:"Bolivia"
      }
    });

    await modal.present();

    //const { data } = await modal.onDidDismiss(); //muestra la data al terminar de cerrar
    const { data } = await modal.onWillDismiss(); //muestra la data al cerrarse el modal
    console.log(data);

  }

}
